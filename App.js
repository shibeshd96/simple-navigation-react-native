/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
      <Text style={styles.linktext}
        onPress = { ()=> navigate('Profile')}>Click me -> Navigate to Profile
        </Text>
      </View>
    );
  }
}

class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
      <Text style={styles.linktext}
        onPress = { ()=> navigate('Home')}>Click me -> Navigate to Home
        </Text>
      </View>
    );
  }
}

const NavigationApp = createStackNavigator(
  {
    Home: { 
      screen: HomeScreen
    },

    Profile: { 
      screen: ProfileScreen 
    } 
  }
);

const App = createAppContainer(NavigationApp);

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
  },

  linktext: {
    marginTop: 20,
    fontSize: 22,
  }
});